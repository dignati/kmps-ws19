package main

import (
	"fmt"
	"time"

	"github.com/visualfc/atk/tk"
)

type Window struct {
	*tk.Window
}

func Compute() {
	time.Sleep(10 * time.Second)
}

func NewWindow() *Window {
	mw := &Window{tk.RootWindow()}
	lbl := tk.NewLabel(mw, "Responsive Example")
	btn := tk.NewButton(mw, "Compute")
	btn.OnCommand(func() {
		fmt.Println("Button clicked")
		Compute()
		fmt.Println("Compute Started")
	})
	tk.NewVPackLayout(mw).AddWidgets(lbl, tk.NewLayoutSpacer(mw, 0, true), btn)
	mw.ResizeN(300, 200)
	return mw
}

func main() {
	tk.MainLoop(func() {
		mw := NewWindow()
		mw.SetTitle("Responsive")
		mw.Center()
		mw.ShowNormal()
	})
}
