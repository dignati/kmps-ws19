package main

import (
	"fmt"
	"time"
)

func main2() {
	for i := 0; i < 9; i++ {
		go func(dp int) {
			fmt.Println("Hello from GoRoutine ", dp, &dp)
			time.Sleep(2 * time.Second)
			fmt.Println("Goroutine ends")
		}(i)
	}
	time.Sleep(10 * time.Second)
	fmt.Println("Done.")
}
