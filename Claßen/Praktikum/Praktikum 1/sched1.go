package main

import (
	"fmt"
	"os"
	"runtime"
	"runtime/trace"
	"time"
)

func f(myName string) {
	for i := 0; i < 10; i++ {
		fmt.Println(myName, ": ", i)
	}
}
func main6() {
	file, err := os.Create("SchedTrace.out")
	if err != nil {
		fmt.Println(err)
		return
	}
	trace.Start(file)
	defer trace.Stop()
	runtime.GOMAXPROCS(1) // ... limitiere die nutzbaren Prozessoren auf einen.
	fmt.Printf("runtime.GOMAXPROCS(0) returned %d CPUs\n", runtime.GOMAXPROCS(0))
	go f("goroutine1")
	go f("goroutine2")
	go f("goroutine3")
	time.Sleep(10 * time.Second)
}
