package main

import "time"

func main5() {
	var x int = 0
	go func() {
		x++
	}()
	go func() {
		x++
	}()
	time.Sleep(5 * time.Second)
}
