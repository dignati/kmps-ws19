package main

import (
	"fmt"
	"time"
)

func main1() {
	for i := 0; i < 9; i++ {
		go func() {
			fmt.Println("Hello from GoRoutine ", i, &i)
			time.Sleep(2 * time.Second)
			fmt.Println("Goroutine ends")
		}()
	}
	time.Sleep(10 * time.Second)
	fmt.Println("Done.")
}
