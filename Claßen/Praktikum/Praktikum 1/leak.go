package main

import (
	"log"
	"runtime"
	"time"
)

func main7() {
	logGoNum()
	var ch chan int
	go func(ch chan int) {
		<-ch
	}(ch)
	for range time.Tick(2 * time.Second) {
		logGoNum()
	}
}

func logGoNum() {
	log.Printf("goroutine number: %d\n", runtime.NumGoroutine())
}
