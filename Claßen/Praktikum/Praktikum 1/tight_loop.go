package main

import (
	"fmt"
	"runtime"
	"time"
)

func main3() {
	var x int
	procs := runtime.GOMAXPROCS(0) - 1
	for i := 0; i < procs; i++ {
		go func() {
			for {
				x++
			}
		}()
	}
	time.Sleep(time.Second)
	fmt.Println("x = ", x)
}
