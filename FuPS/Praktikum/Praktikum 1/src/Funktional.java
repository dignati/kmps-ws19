import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Funktional {
    static class ParseState<T> {
        private ArrayList<String> tokens;
        private int readTokens;
        private T result;
        ParseState(ArrayList<String> tokens) {
            this.tokens = tokens;
        }
        ParseState(ParseState old) {
            this.tokens = old.tokens;
            this.readTokens = old.readTokens;
            this.result = null;
        }
        String nextToken() {
            return this.tokens.get(readTokens);
        }
        String nextToken(int offset) {
            return this.tokens.get(readTokens + offset);
        }
        ParseState<T> read(int n) {
            this.readTokens += n;
            return this;
        }
        ParseState<T> result(T result) {
            this.result = result;
            return this;
        }
        T getResult() {
            return this.result;
        }

        boolean isDone() {
            return readTokens >= tokens.size();
        }
    }

    private static ArrayList<String> createTokenList(byte[] content, String curr, int offset) {
        if (offset == content.length) {
            return new ArrayList<>();
        }
        if(content[offset] == '<' || content[offset] == '>') {
            ArrayList<String> res = new ArrayList<>();
            if(!curr.isBlank()) {
                res.add(curr.trim());
            }
            res.addAll(createTokenList(content, "", offset+1));
            return res;
        } else {
            return createTokenList(content, curr + ((char) content[offset]), offset + 1);
        }
    }

    private static ParseState<Track> parseTrack(ParseState<Track> state) {
        String tag = state.nextToken();
        Track track = state.getResult();
        switch (tag) {
            case "track":
                return parseTrack(state.read(1).result(new Track()));
            case "/track":
                return state.read(1);
            case "title":
                track.title = state.nextToken(1);
                return parseTrack(state.read(2));
            case "length":
                track.length = state.nextToken(1);
                return parseTrack(state.read(2));
            case "rating":
                track.rating = Integer.parseInt(state.nextToken(1));
                return parseTrack(state.read(2));
            case "feature":
                track.features.add(state.nextToken(1));
                return parseTrack(state.read(2));
            case "writing":
                track.writers.add(state.nextToken(1));
                return parseTrack(state.read(2));
            case "/title":
            case "/length":
            case "/writing":
            case "/rating":
            case "/feature":
                return parseTrack(state.read(1));
        }
        return null;
    }

    private static ParseState<Album> parseAlbum(ParseState<Album> state) {
        String tag = state.nextToken();
        Album album = state.getResult();
        switch (tag) {
            case "album":
                return parseAlbum(state.read(1).result(new Album()));
            case "track":
                ParseState<Track> trackParseState = parseTrack(new ParseState<Track>(state));
                album.tracks.add(trackParseState.getResult());
                return parseAlbum(new ParseState<Album>(trackParseState).result(album));
            case "artist":
                album.artist = state.nextToken(1);
                return parseAlbum(state.read(2));
            case "title":
                album.title = state.nextToken(1);
                return parseAlbum(state.read(2));
            case "date":
                album.date = state.nextToken(1);
                return parseAlbum(state.read(2));
            case "/album":
                return state.read(1);
            case "/track":
            case "/artist":
            case "/title":
            case "/date":
                return parseAlbum(state.read(1));
        }
        return null;
    }

    private static ArrayList<Album> parseFile(ParseState<Album> state) {
        if(state.isDone()) {
            return new ArrayList<Album>();
        } else {
            ArrayList<Album> res = new ArrayList<>();
            ParseState<Album> nextState = parseAlbum(state);
            res.add(nextState.getResult());
            res.addAll(parseFile(nextState));
            return res;
        }
    }

    private static ArrayList<Album> parseFile(ArrayList<String> tokens) {
        return parseFile(new ParseState<Album>(tokens));
    }

    public static void main(String[] args) throws IOException {
        byte[] content = Files.readAllBytes(Paths.get("Praktikum/Praktikum 1/alben.xml"));
        ArrayList<String> tokens = createTokenList(content, "", 0);
        ArrayList<Album> albums = parseFile(tokens);

        albums.forEach(System.out::println);
    }
}
