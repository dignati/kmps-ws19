import scala.io.Source

case class Track(title: String, length: String, rating: Int, features: List[String], writers: List[String])

case class Album(title: String, date: String, artist: String, tracks: List[Track])

def read(): List[Char] = Source.fromFile("Praktikum/Praktikum 3/alben.xml").toList

def createTokenList(chars: List[Char], token: String = ""): List[String] = chars match {
  case Nil => Nil
  case '<' :: rest => token.trim match {
    case "" => createTokenList(rest)
    case t => t :: createTokenList(rest)
  }
  case '>' :: rest => token.trim :: createTokenList(rest)
  case char :: rest => createTokenList(rest, token + char)
}

def defaultTrack(): Track = Track("", "", 0, List(), List())
def defaultAlbum(): Album = Album("", "", "", List())

def parseTrack(tokens: List[String], track: Track = defaultTrack()): (List[String], Track) = tokens match {
  case "title" :: title :: "/title" :: rest => parseTrack(rest, track.copy(title = title))
  case "length" :: length :: "/length" :: rest => parseTrack(rest, track.copy(length = length))
  case "rating" :: rating :: "/rating" :: rest => parseTrack(rest, track.copy(rating = rating.toInt))
  case "feature" :: feature :: "/feature" :: rest => parseTrack(rest, track.copy(features = track.features :+ feature))
  case "writing" :: writer :: "/writing" :: rest => parseTrack(rest, track.copy(writers = track.writers :+ writer))
  case _ :: rest => (rest, track)
  case Nil => (Nil, track)
}
def parseAlbum(tokens: List[String], album: Album = defaultAlbum()): (List[String], Album) = tokens match {
  case "title" :: title :: "/title" :: rest => parseAlbum(rest, album.copy(title = title))
  case "date" :: date :: "/date" :: rest => parseAlbum(rest, album.copy(date = date))
  case "artist" :: artist :: "/artist" :: rest => parseAlbum(rest, album.copy(artist = artist))
  case "track" :: rest =>
    val (tokens, track) = parseTrack(rest)
    parseAlbum(tokens, album.copy(tracks = album.tracks :+ track))
  case _ :: rest => (rest, album)
  case Nil => (Nil, album)
}

def parseFile(tokens: List[String], albums: List[Album] = List()): List[Album] = tokens match {
  case "album" :: tokens =>
    val (rest, album) = parseAlbum(tokens)
    parseFile(rest, albums :+ album)
  case _ => albums
}

val alben = parseFile(createTokenList(read()))
val tracks = alben.flatMap(_.tracks)

def map[A](func: A => A, input_list: List[A]): List[A] = input_list match {
  case ::(head, next) => func(head) :: map(func, next)
  case Nil => Nil
}

print(map((album: Album) => album.copy(title = album.title.toUpperCase()), alben))

def upperCaseTrackTitle(tracks: List[Track]): List[Track] =
  map((track: Track) => track.copy(title = track.title.toUpperCase), tracks)

def upperCaseTrackTitleInAlbum(album: Album): Album =
  album.copy(tracks = upperCaseTrackTitle(album.tracks))

print(map(upperCaseTrackTitleInAlbum, alben))

def poly_map[A, B](func: A => B, input_list: List[A]): List[B] = input_list match {
  case ::(head, next) => func(head) :: poly_map(func, next)
  case Nil => Nil
}

print(poly_map((album: Album) => poly_map((track: Track) => track.length, album.tracks), alben))

def filter[A](condition: A => Boolean, input_list: List[A]): List[A] = input_list match {
  case x :: xs => if (condition(x)) x :: filter(condition, xs) else filter(condition, xs)
  case Nil => Nil
}

def even(x: Int): Boolean = x % 2 == 0

filter(even, List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))

poly_map((track: Track) => track.writers, tracks)

poly_map((track: Track) => track.title,
  filter((track: Track) => track.writers.contains("Rod Temperton"), tracks))

def isGoodTrack(track: Track): Boolean = track.rating >= 4
def goodTracks = filter(isGoodTrack, tracks)

print(goodTracks)

def partition[A](condition: A => Boolean, input_list: List[A], part: List[A] = Nil): List[List[A]] = input_list match {
  case x :: xs => if (condition(x)) {
    part :: partition(condition, xs)
  } else {
    partition(condition, xs, part :+ x)
  }
  case Nil => part :: Nil
}

partition(even, List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
partition((c: Char) => c.isUpper, List('a', 'b', 'c', 'D', 'e', 'f', 'G', 'H', 'i', 'J'))

print(partition((track: Track) => track.title == "Thriller", tracks))

def newCreateTokenList(chars: List[Char], token: String = ""): List[String] =
  filter((token: String) => !token.isBlank,
    poly_map((chars: List[Char]) => chars.mkString,
      partition((c: Char) => c == '<' || c == '>', chars)))

print(parseFile(newCreateTokenList(read())))

def mapReduceRangeR(reduce: (Int, Int) => Int, id: Int)(map: Int => Int)(start: Int, end: Int): Int =
  if (start > end) {
    id
  } else {
    reduce(map(start), mapReduceRangeR(reduce, id)(map)(start + 1, end))
  }

def sum = mapReduceRangeR(_ + _, 0) _
def prod = mapReduceRangeR(_ * _, 1) _
def fak = prod(x => x)(1, _)
print(sum(x => x)(1, 5))
print(fak(6))

def mapReduceRangeAlt(r: (Int, Int) => Int)(f: Int => Int)(start: Int, end: Int) =
  (start to end).map(f).reduce(r)

mapReduceRangeAlt(_ - _)((x: Int)=> x)(1, 3)
