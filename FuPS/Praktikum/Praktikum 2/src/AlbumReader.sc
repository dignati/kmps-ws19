import scala.io.Source

case class Track(title: String, length: String, rating: Int, features: List[String], writers: List[String])
case class Album(title:String, date: String, artist: String, tracks: List[Track])

def read(): List[Char] = Source.fromFile("Praktikum/Praktikum 2/alben.xml").toList

def createTokenList(chars: List[Char], token: String = ""): List[String] = chars match {
  case Nil => Nil
  case '<'::rest => token.trim match {
    case "" => createTokenList(rest)
    case t => t::createTokenList(rest)
  }
  case '>'::rest => token.trim :: createTokenList(rest)
  case char::rest => createTokenList(rest, token + char)
}

def defaultTrack(): Track = Track("", "", 0, List(), List())
def defaultAlbum(): Album = Album("", "", "", List())

def parseTrack(tokens: List[String], track: Track = defaultTrack()): (List[String], Track) = tokens match {
  case "title"::title::"/title"::rest => parseTrack(rest, track.copy(title = title))
  case "length"::length::"/length"::rest => parseTrack(rest, track.copy(length = length))
  case "rating"::rating::"/rating"::rest => parseTrack(rest, track.copy(rating = rating.toInt))
  case "feature"::feature::"/feature"::rest => parseTrack(rest, track.copy(features = track.features:+feature))
  case "writing"::writer::"/writing"::rest => parseTrack(rest, track.copy(writers = track.writers:+writer))
  case _::rest => (rest, track)
  case Nil => (Nil, track)
}
def parseAlbum(tokens: List[String], album: Album = defaultAlbum()): (List[String], Album) = tokens match {
  case "title"::title::"/title"::rest => parseAlbum(rest, album.copy(title = title))
  case "date"::date::"/date"::rest => parseAlbum(rest, album.copy(date = date))
  case "artist"::artist::"/artist"::rest => parseAlbum(rest, album.copy(artist = artist))
  case "track"::rest => {
    val (tokens, track) = parseTrack(rest)
    parseAlbum(tokens, album.copy(tracks = album.tracks:+track))
  }
  case _::rest =>  (rest, album)
  case Nil => (Nil, album)
}

def parseFile(tokens: List[String], albums: List[Album] = List()): List[Album] = tokens match {
  case "album"::tokens => {
    val (rest, album) = parseAlbum(tokens)
    parseFile(rest, albums:+album)
  }
  case _ => albums
}

parseFile(createTokenList(read()))

def prod(f: Int => Int, a: Int, b: Int): Int = {
  if(a > b) {
    1
  } else {
    f(a) * prod(f, a + 1, b)
  }
}

def prodCurry(f: Int => Int): (Int, Int) => Int = {
  (a, b) => prod(f, a, b)
}

def prodBetween = prodCurry(x => x)
prodBetween(3, 6)
def fak(x: Int) = prodBetween(1, x)
fak(5)