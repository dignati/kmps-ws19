abstract class BinTree
case class TreeNode(elem: Any, left: BinTree, right: BinTree) extends BinTree
case object EmptyNode extends BinTree