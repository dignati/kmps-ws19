// Append

def append(xs: List[Any], ys: List[Any]): List[Any] = xs match {
  case head :: next => head::append(next, ys)
  case Nil => ys
}

append(1::2::3::Nil, 3::4::Nil)

// Binary Tree

TreeNode(3, TreeNode(1, EmptyNode, EmptyNode), EmptyNode)

// Fibonacci

def fib(x: Int): Int = if (x < 2) {
  x
} else {
  fib(x - 1) + fib(x - 2)
}

fib(20)

// Größter geimeinsamer Teiler

def ggt(a: Int, b: Int): Int = (a,b) match {
  case (a,0) => a
  case (a,b) => ggt(b, a % b)
}

ggt(36,16)

// IsSorted

def isSorted(xs: List[Int]): Boolean = xs match {
  case Nil => true
  case x::Nil => true
  case x::y::rest => (x < y) && isSorted(rest)
}

isSorted(1::4::3::Nil)