def präfix(xs: List[Any], ys: List[Any]) : Boolean = (xs, ys) match {
  case (_, Nil) => true
  case (Nil, _) => false
  case (x::xs, y::ys) => x == y && präfix(xs, ys)
}

präfix(Nil, 1::2::Nil)
präfix(1::2::Nil, Nil)
präfix(1::2::Nil, 1::2::Nil)
präfix(1::2::3::Nil, 1::2::Nil)


def attach(xs: List[Any], y: Any): List[Any] = xs match {
  case Nil => y::Nil
  case x::xs => x::attach(xs, y)
}

attach(Nil, 1)
attach(1::2::3::Nil, 4)

def anzKnoten(tree: BinTree): Int = tree match {
  case EmptyNode => 0
  case TreeNode(_, left, right) => 1 + anzKnoten(left) + anzKnoten(right)
}

anzKnoten(TreeNode(1, EmptyNode, TreeNode(1, EmptyNode, EmptyNode)))

def präorder(tree: BinTree): List[Any] = tree match {
  case EmptyNode => Nil
  case TreeNode(elem, left, right) => elem::(präorder(left) ++ präorder(right))
}

präorder(TreeNode(1, EmptyNode, TreeNode(3, TreeNode(2, EmptyNode, EmptyNode), EmptyNode)))


def fibImpl(x: Int, y: Int, nth: Int, limit: Int): Int = {
  if (nth == limit) {
    x
  } else {
    fibImpl(y, x+y, nth + 1, limit)
  }
}

def fib(x: Int): Int = {
  fibImpl(0,1,0,x)
}

fib(0)
fib(1)
fib(2)
fib(3)

def sum(a: Int, b: Int): Int = {
  if (a == b) {
    a
  } else {
    a + sum(a+1, b)
  }
}

sum(1,3)

def root(x: Double, y: Double = 1, loop: Int = 10): Double = {
  if(loop > 0) {
    root(x, (y + x/y)/2, loop - 1)
  } else {
    y
  }
}

root(2)